from django.db import models

# Create your models here.
class Blog(models.Model):
    title = models.CharField(max_length=150)
    description = models.TextField()
    date_created = models.DateTimeField(auto_now=False, auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    #url = models.URLField()
    
    def __str__(self):
        return self.title
